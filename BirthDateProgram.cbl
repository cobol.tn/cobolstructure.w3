       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BirthDateProgram.
       AUTHOR. Sherlock Holmes.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  BirthDate.
           05 YearOfBirth.
              10 CenturyOB   PIC 99.
              10 YearOB      PIC 99.
           05 MonthOfBirth   PIC 99.
           05 DayOfBirth     PIC 99.
       PROCEDURE DIVISION.
       DisplayBirthDay.
           MOVE 19990803 TO BirthDate
           DISPLAY "Month is " MonthOfBirth
           DISPLAY "Century of birth is " CenturyOB 
           DISPLAY "Year of birth is " YearOB
           DISPLAY DayOfBirth "/" MonthOfBirth "/" YearOfBirth
           MOVE ZEROS TO YearOfBirth 
           DISPLAY "Birth date is "BirthDate .
