       IDENTIFICATION DIVISION. 
       PROGRAM-ID. ConditionNames.
       AUTHOR. Sherlock Holmes.
      * Using condition names (level 88's) and the EVALUATE
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CharIn               PIC   X.
           88 Vowel             VALUE "a", "e", "i", "o", "u".
           88 Consonant         VALUE "b", "c", "d", "f", "g", "h"
                                      "j" THROUGH "n", "p" THROUGH "t"
                                      "v" THROUGH "z".
           88 Digit             VALUE "0" THROUGH "9".
           88 ValidCharacter    VALUE "a" THROUGH "z", "0" THROUGH "9".
       PROCEDURE DIVISION.
       Begin.
           DISPLAY "Enter lower case character or digit, Invalid char".
           ACCEPT CharIn  
           PERFORM UNTIL NOT  ValidCharacter 
               EVALUATE TRUE 
                 WHEN Vowel      DISPLAY 
                                 "The letter " CharIn " is a vowel"
                 WHEN Consonant  DISPLAY 
                                 "The letter " CharIn " is a Consonant" 
                 WHEN Digit      DISPLAY 
                                 "The letter " CharIn " is a Digit"
               END-EVALUATE 
               ACCEPT CharIn 
           END-PERFORM
           STOP RUN.
       