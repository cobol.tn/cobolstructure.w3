       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DoCalc.
       AUTHOR. Sherlock Holmes.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  First-Num   PIC 9     VALUE  0.
       01  SecondNum   PIC 9     VALUE  0.
       01  CalcResult  PIC 99    VALUE  ZEROS.
       01  UserPrompt  PIC X(38) VALUE 
                       "Plase enter two single digit numbers.".
       PROCEDURE DIVISION.
       CalculateResult.
           DISPLAY UserPrompt.
           ACCEPT First-Num.
           ACCEPT SecondNum.
           COMPUTE CalcResult = First-Num + SecondNum.
           DISPLAY "Result is = ", CalcResult, ".".
           STOP RUN.
